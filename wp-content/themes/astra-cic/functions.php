<?php
/**
 * Connected in Crisis Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Connected in Crisis
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_CONNECTED_IN_CRISIS_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'connected-in-crisis-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_CONNECTED_IN_CRISIS_VERSION, 'all' );
    wp_enqueue_style( 'imagemap', get_stylesheet_directory_uri() . '/map/image-map-pro.min.css', array('connected-in-crisis-theme-css'), CHILD_THEME_CONNECTED_IN_CRISIS_VERSION, 'all' );
    
    wp_enqueue_script( 'imagemap', get_stylesheet_directory_uri() . '/map/image-map-pro.min.js', array( 'jquery' ), CHILD_THEME_ILOCAL_VERSION, true );
    wp_enqueue_script( 'imagemap-regions', get_stylesheet_directory_uri() . '/map/regions.js', array( 'imagemap' ), CHILD_THEME_ILOCAL_VERSION, true );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );